import React, { Component } from 'react'
import './header.css'

export default class header extends Component {
    render() {
        return (
            <header>
                <div className="container">
                    <p>minionStore</p>
                    <div>
                        <ul>
                            <li>Carrinho</li>
                            <li>Contato</li>
                            <li>Registre-se</li>
                            <li>Login</li>
                        </ul>
                    </div>
                </div>
            </header>
        )
    }
}
